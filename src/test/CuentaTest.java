package test;

/**
 * Tester: Chavez Morelos Noe Baldemar.
 * Fecha: 08/11/2020.
 * Tecnicas: 
 * Complejidad ciclomatica (Cobertura de Ramas). 
 * partincion de equivalencias. 
 * Analisis de valores limite.
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import errors.ExcepcionCuenta;
import modelo.Cuenta;

public class CuentaTest {
	
	private Cuenta cuenta;
	static Logger logger = Logger.getLogger("class CuentaTest");
	DecimalFormat decimalFormat = new DecimalFormat("#.00");
	
	@BeforeEach
	public void beforeEach() throws ExcepcionCuenta {
		logger.log(Level.INFO, "beforeEach()");
		cuenta = new Cuenta(50.56);  
	}
	
	@AfterEach
	public void afterEach() {
		logger.log(Level.INFO, "afterEach");
		cuenta = null;
	}
	

	/**
	 * Aplicando pruebas con la tecnica de complejidad ciclom�tica para hallar todos los
	 * caminos (ramas) posibles del c�digo. 
	 */
	
	@Test
	public void testCuentaRama1() {
		logger.log(Level.INFO, "TestCuentaRama1");
		double actual = cuenta.consulta();
		double esperado = 50.56;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testCuentaRama2() {
		logger.log(Level.INFO, "TestCuentaRama2");
		assertThrows(ExcepcionCuenta.class, () -> new Cuenta(-50.56), "Error: valor menor a cero");
	}
	
	@Test
	public void testDepositarRama1() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testDepositarRama1");
		cuenta.depositar(500);
		double esperado = 550.56;
		double actual = cuenta.consulta();
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testDepositarRama2() {
		logger.log(Level.INFO, "testDepositarRama2");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(50.56).depositar(-500), "Error: valor menor a cero");
	}
	
	@Test
	public void testRetirarRama1() {
		logger.log(Level.INFO, "testRetirarRama1");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(50.56).retirar(-500), "Error: valor menor a cero");
	}
	
	@Test
	public void testRetirarRama2() {
		logger.log(Level.INFO, "testRetirarRama2");
		assertThrows(ExcepcionCuenta.class, 
				() -> {
					cuenta = new Cuenta(50.56);
					cuenta.depositar(300);
					cuenta.retirar(500);
				}, "Error, Saldo insuficiente");
	}
	
	@Test
	public void testRetirarRama3() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testRetirarRama3");
		cuenta.depositar(500);
		cuenta.retirar(500);
		double actual = cuenta.consulta();
		double esperado = 50.56;
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		assertEquals(decimalFormat.format(esperado), decimalFormat.format(actual));
		//assertEquals(esperado, actual);
	}
	
	/**
	 * Aplicando purebas con la t�cnicas partici�n de equivalencias.
	 * @throws ExcepcionCuenta 
	 */
	@Test
	public void testCuentaValorMayorCeroIgual() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testCuentaValorMayorCeroIgual");
		Cuenta c = new Cuenta(50);
		double actual = c.consulta();
		double esperado = 50;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testCuentaValorMenorIgualCero() {
		logger.log(Level.INFO, "testCuentaValorMenorIgualCero");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(-50));
	}
	
	@Test 
	void testDepositaValorMayorIgualCero() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testDepositaValorMayorIgualCero");
		cuenta.depositar(500);
		double actual = cuenta.consulta();
		double esperado = 550.56;
		assertEquals(esperado, actual);
	}
	
	@Test 
	void testDepositaValorMenorCero() {
		logger.log(Level.INFO, "testDepositaValorMenorCero");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(0).depositar(-500));
	}
	
	@Test
	public void testRetirarValorMayorIgualCero() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testRetirarValorMayorIgualCero");
		Cuenta c = new Cuenta(500);
		c.retirar(100);
		double actual = c.consulta();
		double esperado = 400;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testRetirarValorMenorCero() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testRetirarValorMenorCero");
		assertThrows(ExcepcionCuenta.class, 
				() -> {
					Cuenta c = new Cuenta(0);
					c.retirar(-100);
				});
	}
	
	@Test
	public void testRetirarValorMayorSaldo() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testRetirarValorMayorSaldo");
		assertThrows(ExcepcionCuenta.class, 
				() -> {
					Cuenta c = new Cuenta(0);
					c.retirar(100);
				});
	}
	
	
	/**
	 * Pruebas con la tecnica de valores limite.
	 * @throws ExcepcionCuenta 
	 */
	@Test
	public void testConstructorValorCero() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testConstructorValorCero");
		Cuenta c = new Cuenta(0);
		double actual = c.consulta();
		double esperado = 0;
		assertEquals(esperado, actual);
	}
	
	@Test 
	public void testConstructorValorMenosUno() {
		logger.log(Level.INFO, "testConstructorValorMenosUno");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(-1));
	}
	
	@Test 
	public void testDepositarValorIgualCero() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testConstructorValorMenosUno");
		Cuenta c = new Cuenta(0);
		c.depositar(0);
		double actual = c.consulta();
		double esperado = 0;
		assertEquals(esperado, actual);
	}
	
	@Test 
	public void testDepositarValorIgualMenosUno() {
		logger.log(Level.INFO, "testDepositarValorIgualMenosUno");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(0).depositar(-1));
	}
	
	@Test 
	public void testRetirarValorCero() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testRetirarValorCero");
		Cuenta c = new Cuenta(500);
		c.retirar(0);
		double actual = c.consulta();
		double esperado = 500;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testRetirarValorSaldo() throws ExcepcionCuenta {
		logger.log(Level.INFO, "testRetirarValorSaldo");
		Cuenta c = new Cuenta(500);
		c.retirar(500);
		double actual = c.consulta();
		double esperado = 0;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testRetirarValorMenosUno() {
		logger.log(Level.INFO, "testRetirarValorMenosUno");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(0).retirar(-1));
	}
	
	@Test 
	void testRetirarValorSaldoMasUno() {
		logger.log(Level.INFO, "testRetirarValorSaldoMasUno");
		assertThrows(ExcepcionCuenta.class, 
				() -> new Cuenta(500).retirar(501));
	}
	
}
