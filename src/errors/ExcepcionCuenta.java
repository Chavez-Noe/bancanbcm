package errors;

public class ExcepcionCuenta extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ExcepcionCuenta(String mensaje) {
		super(mensaje);
	}
}
