package modelo;

import errors.ExcepcionCuenta;

public class Cuenta {
	private double saldo;
	
	public Cuenta(double saldoInicial) throws ExcepcionCuenta {
		if (saldoInicial>=0)
			saldo= saldoInicial;
		else 
			throw new ExcepcionCuenta("El saldo inicial es un valor inválido negativo");
	}
	
	public double consulta() {
		return saldo;
	}
	
	public void depositar(double cantidad) throws ExcepcionCuenta{
		if (cantidad>=0)
			saldo = saldo + cantidad;
		else 
			throw new ExcepcionCuenta("La cantidad es un valor inválido negativo");
	}

	public void retirar(double cantidad) throws ExcepcionCuenta {
		if (cantidad>=0 && cantidad <= saldo)
			saldo = saldo - cantidad;
		else 
			throw new ExcepcionCuenta("La cantidad es inválida, o es negativo o es mayor que el saldo");
	}
}
